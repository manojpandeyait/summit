create database usersdb;

CREATE TABLE usersdb.`users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
   `role` varchar(100) NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ;


CREATE TABLE usersdb.`region`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(100) NOT NULL,
`rpic` varchar(100) ,
PRIMARY KEY (`id`)
)  ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ;

CREATE TABLE usersdb.`industry`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(100) NOT NULL,
`ipic` varchar(100) ,
PRIMARY KEY (`id`)
)  ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ;

CREATE TABLE usersdb.`client`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(100) NOT NULL,
`region_id` int(11) ,
`industry_id` int(11) ,
PRIMARY KEY (`id`),
KEY `fk_region` (`region_id`),
KEY `fk_industry` (`industry_id`),
CONSTRAINT `fk_region` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`),
CONSTRAINT `fk_industry` FOREIGN KEY (`industry_id`) REFERENCES `industry` (`id`)
)  ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ;


CREATE TABLE usersdb.`target_client`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`current_annual_revenue` Decimal NOT NULL,
`potential_annual_revenue` Decimal NOT NULL,
`user_id` int(11) NOT NULL,
`client_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_user` (`user_id`),
 KEY `fk_client` (`client_id`),
CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
CONSTRAINT `fk_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ;

CREATE TABLE usersdb.`target_prospect`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(100) NOT NULL,
`current_annual_revenue` Decimal NOT NULL,
`potential_annual_revenue` Decimal NOT NULL,
`region_id` int(11) ,
`industry_id` int(11) ,
`user_id` int(11) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_target_prospect_region` (`region_id`),
 KEY `fk_target_prospect_industry` (`industry_id`),
 KEY `fk_target_prospect_user` (`user_id`),
CONSTRAINT `fk_target_prospect_region` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`),
CONSTRAINT `fk_target_prospect_industry` FOREIGN KEY (`industry_id`) REFERENCES `industry` (`id`),
CONSTRAINT `fk_target_prospect_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ;

CREATE TABLE usersdb.`action_plan` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`description` varchar(100) NOT NULL,
`target_client_id` int(11) ,
`target_prospect_id` int(11) ,
PRIMARY KEY (`id`),
KEY `fk_action_plan_target_client` (`target_client_id`),
KEY `fk_action_plan_target_prospect` (`target_prospect_id`),
CONSTRAINT `fk_action_plan_target_client` FOREIGN KEY (`target_client_id`) REFERENCES `target_client` (`id`),
CONSTRAINT `fk_action_plan_target_prospect` FOREIGN KEY (`target_prospect_id`) REFERENCES `target_prospect` (`id`)
)  ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ;

CREATE TABLE usersdb.`helper` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(100) NOT NULL,
`target_client_id` int(11) ,
`target_prospect_id` int(11) ,
PRIMARY KEY (`id`),
KEY `fk_helper_client` (`target_client_id`),
KEY `fk_helper_target_prospect` (`target_prospect_id`),
CONSTRAINT `fk_helper_client` FOREIGN KEY (`target_client_id`) REFERENCES `target_client` (`id`),
CONSTRAINT `fk_helper_target_prospect` FOREIGN KEY (`target_prospect_id`) REFERENCES `target_prospect` (`id`)
)  ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ;



insert into usersdb.`users` (username,password,ROLE) values ('user','12dea96fec20593566ab75692c9949596833adc9','ROLE_USER');
insert into usersdb.`users` (username,password,ROLE) values ('admin','d033e22ae348aeb5660fc2140aec35850c4da997','ROLE_ADMIN');

insert into usersdb.`region` (id,name,rpic) values (1,'MN','MN-RPIC');
insert into usersdb.`region` (id,name,rpic) values (2,'WI','WI-RPIC');

insert into usersdb.`industry` (id,name,ipic) values (1,'Accounting','Accounting-IPIC');
insert into usersdb.`industry` (id,name,ipic) values (2,'Insurance','Insurance-IPIC');

insert into usersdb.`client` (id,name,region_id,industry_id) values (1,'wipfli',1,1);
insert into usersdb.`client` (id,name,region_id,industry_id) values (2,'cmic',2,2);


delete from usersdb.`action_plan` ;
delete from usersdb.`helper`;
delete from usersdb.`target_prospect`;
delete from usersdb.`target_client`;




drop table usersdb.`action_plan`;
drop table usersdb.`helper`;
drop table usersdb.`target_prospect`;
drop table usersdb.`target_client`;
drop table usersdb.`client`;
drop TABLE usersdb.`industry`;
drop TABLE usersdb.`region`;
drop table usersdb.`users`;

