package org.wipfli.summit.domain;

public class UserData {
	private TargetClient targetClient;
	private TargetProspect targetProspect;


	public UserData(){
		
	}

	public UserData(TargetClient targetClient,TargetProspect targetProspect){
		this.targetClient=targetClient;
		this.targetProspect=targetProspect;
	}
	public TargetClient getTargetClient() {
		return targetClient;
	}
	public void setTargetClient(TargetClient targetClient) {
		this.targetClient = targetClient;
	}
	public TargetProspect getTargetProspect() {
		return targetProspect;
	}
	public void setTargetProspect(TargetProspect targetProspect) {
		this.targetProspect = targetProspect;
	}
}
