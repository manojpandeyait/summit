package org.wipfli.summit.domain;

public class ActionPlan {
	private int id;
	private String description;	
	public ActionPlan() {}
	
	public ActionPlan(String description) { this.description = description; }
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean equals(Object obj) { 
		if (obj == null) return false; 
		if (!this.getClass().equals(obj.getClass())) return false; 
		ActionPlan obj2 = (ActionPlan)obj; 
		if((this.id == obj2.getId()) && (this.description.equals(obj2.getDescription()))) { return true; } return false; }
	 
	public int hashCode() {
		int tmp = 0;
		tmp = ( id + description ).hashCode();
		return tmp; }
	}


