package org.wipfli.summit.domain;

public class Region {
	
	private int id;
	private String name;
	private String rpic;
	
	public Region(){
		
	}	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRpic() {
		return rpic;
	}
	public void setRpic(String rpic) {
		this.rpic = rpic;
	}	
}
