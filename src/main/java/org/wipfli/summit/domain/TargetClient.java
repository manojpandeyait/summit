package org.wipfli.summit.domain;

import java.math.BigDecimal;
import java.util.Set;

public class TargetClient {
	private int id;
	private BigDecimal currentAnnualRevenue;
	private BigDecimal potentialAnnualRevenue;
	private Client client;
	private User user;

	private Set<ActionPlan> actionPlans;
	private Set<Helper> helpers;
	
	public TargetClient() {}
	
	public TargetClient(BigDecimal currentAnnualRevenue,BigDecimal potentialAnnualRevenue,Client client,User user) {
		
		this.currentAnnualRevenue=currentAnnualRevenue;
		this.potentialAnnualRevenue=potentialAnnualRevenue;
		this.client=client;
		this.user=user;
	}
	
   public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public BigDecimal getCurrentAnnualRevenue() {
		return currentAnnualRevenue;
	}
	public void setCurrentAnnualRevenue(BigDecimal currentAnnualRevenue) {
		this.currentAnnualRevenue = currentAnnualRevenue;
	}
	public BigDecimal getPotentialAnnualRevenue() {
		return potentialAnnualRevenue;
	}
	public void setPotentialAnnualRevenue(BigDecimal potentialAnnualRevenue) {
		this.potentialAnnualRevenue = potentialAnnualRevenue;
	}
	
	public Set<ActionPlan> getActionPlans() {
		return actionPlans;
	}

	public void setActionPlans(Set<ActionPlan> actionPlans) {
		this.actionPlans = actionPlans;
	}

	public Set<Helper> getHelpers() {
		return helpers;
	}

	public void setHelpers(Set<Helper> helpers) {
		this.helpers = helpers;
	}
	
}
