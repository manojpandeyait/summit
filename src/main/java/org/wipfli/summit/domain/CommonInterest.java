package org.wipfli.summit.domain;

import java.math.BigDecimal;

public class CommonInterest {
private String name;
private BigDecimal currentAnnualRevenue;
private BigDecimal potentialAnnualRevenue;

public CommonInterest(){
	
}

public CommonInterest(String name, BigDecimal currentAnnualRevenue,BigDecimal potentialAnnualRevenue){
	this.name=name;
	this.currentAnnualRevenue=currentAnnualRevenue;
	this.potentialAnnualRevenue=potentialAnnualRevenue;
}


public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public BigDecimal getCurrentAnnualRevenue() {
	return currentAnnualRevenue;
}
public void setCurrentAnnualRevenue(BigDecimal currentAnnualRevenue) {
	this.currentAnnualRevenue = currentAnnualRevenue;
}
public BigDecimal getPotentialAnnualRevenue() {
	return potentialAnnualRevenue;
}
public void setPotentialAnnualRevenue(BigDecimal potentialAnnualRevenue) {
	this.potentialAnnualRevenue = potentialAnnualRevenue;
}
}
