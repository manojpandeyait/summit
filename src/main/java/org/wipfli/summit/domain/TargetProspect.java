package org.wipfli.summit.domain;

import java.math.BigDecimal;
import java.util.Set;

public class TargetProspect {
	
	private int id;
	private BigDecimal currentAnnualRevenue;
	private BigDecimal potentialAnnualRevenue;
	private String name;	
	private User user;
	private Set<ActionPlan> actionPlans;	
	private Set<Helper> helpers;
	private Region region;	
	private Industry industry;	

	public TargetProspect() {}
	
	public TargetProspect(BigDecimal currentAnnualRevenue,BigDecimal potentialAnnualRevenue,String name,User user) {
		
		this.currentAnnualRevenue=currentAnnualRevenue;
		this.potentialAnnualRevenue=potentialAnnualRevenue;
		this.name=name;
		this.user=user;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public BigDecimal getCurrentAnnualRevenue() {
		return currentAnnualRevenue;
	}
	public void setCurrentAnnualRevenue(BigDecimal currentAnnualRevenue) {
		this.currentAnnualRevenue = currentAnnualRevenue;
	}
	public BigDecimal getPotentialAnnualRevenue() {
		return potentialAnnualRevenue;
	}
	public void setPotentialAnnualRevenue(BigDecimal potentialAnnualRevenue) {
		this.potentialAnnualRevenue = potentialAnnualRevenue;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	
	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}
	
	public Set<ActionPlan> getActionPlans() {
		return actionPlans;
	}

	public void setActionPlans(Set<ActionPlan> actionPlans) {
		this.actionPlans = actionPlans;
	}

	public Set<Helper> getHelpers() {
		return helpers;
	}

	public void setHelpers(Set<Helper> helpers) {
		this.helpers = helpers;
	}


}
