package org.wipfli.summit.dao;

import java.util.List;







import org.wipfli.summit.domain.Client;
import org.wipfli.summit.domain.CommonInterest;
import org.wipfli.summit.domain.Region;
import org.wipfli.summit.domain.User;
import org.wipfli.summit.domain.UserData;
import org.wipfli.summit.dto.ClientDTO;


public interface UserDAO {
	public List<User> list();
	public User getUser(String username) ;	
	public UserData addTargetClient();
	public List<Client>  getAvailableClients();
	public List<CommonInterest> getPeopleHavingSamePreference();
	public List<Region> getRegions();
	public void deleteTargetClient(Integer targetClientId);
	public void addRegion(Region region);
}
