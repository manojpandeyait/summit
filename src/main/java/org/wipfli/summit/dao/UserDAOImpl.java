package org.wipfli.summit.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;




















import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import org.wipfli.summit.domain.ActionPlan;
import org.wipfli.summit.domain.Client;
import org.wipfli.summit.domain.CommonInterest;
import org.wipfli.summit.domain.Helper;
import org.wipfli.summit.domain.Industry;
import org.wipfli.summit.domain.Region;
import org.wipfli.summit.domain.TargetClient;
import org.wipfli.summit.domain.TargetProspect;
import org.wipfli.summit.domain.User;
import org.wipfli.summit.domain.UserData;
import org.wipfli.summit.dto.ClientDTO;

public class UserDAOImpl implements UserDAO {
	private SessionFactory sessionFactory;

	public UserDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public List<User> list() {
		@SuppressWarnings("unchecked")
		List<User> listUser = (List<User>) sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

		return listUser;
	}
	
	@Transactional
	public void addRegion(Region region){
		 sessionFactory.getCurrentSession().save(region);
	}
	
	@Override
	@Transactional
	public User getUser(String username) {
		@SuppressWarnings("unchecked")
		User user = (User) sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("username",username)).list().get(0);

		return user;
	}
	
	@Transactional
	public List<Client> getAvailableClients(){
		
		List<Client> listClient = (List<Client>)sessionFactory.getCurrentSession()
		.createCriteria(Client.class).list();
		//List<ClientDTO> clientListDTO=new ArrayList<ClientDTO>();
		for(Client cli : listClient){
		//	ClientDTO cliDTO=new ClientDTO(cli.getId(),cli.getName(),cli.getRegion(),cli.getIndustry());
		//	Region r= cli.getRegion();			
			//System.out.println("The region ="+r.getName());
			//Industry i=cli.getIndustry();
			//System.out.println("The Industry ="+i.getName());
			//clientListDTO.add(cliDTO);
			
		}
		return listClient;
	}
	
	
	@Transactional
	public List<Region> getRegions(){
		return sessionFactory.getCurrentSession()
		.createCriteria(Region.class).list();
	}
	
	@Transactional
	public List<CommonInterest> getPeopleHavingSamePreference(){
		String username="user";
		User user = (User) sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("username",username)).list().get(0);
		
		TargetClient targetClient=(TargetClient)sessionFactory.getCurrentSession()
		.createCriteria(TargetClient.class).add(Restrictions.eq("user",user)).list().get(0);
		
		Client client=targetClient.getClient();
		
		List<TargetClient> listOtherTargetClient=(List<TargetClient>)sessionFactory.getCurrentSession()
		.createCriteria(TargetClient.class).add(Restrictions.eq("client",client)).list();
		List<CommonInterest> listCommonInterest=new ArrayList<CommonInterest>();
		for(TargetClient cli : listOtherTargetClient){
			listCommonInterest.add(new CommonInterest(cli.getUser().getUsername(),cli.getCurrentAnnualRevenue(),cli.getPotentialAnnualRevenue()));
		}
	        return listCommonInterest;
	}
	
	
	@Transactional
	public void deleteTargetClient(Integer targetClientId){
			
		String hql = "from TargetClient where id= :id"; 
		TargetClient targetClient=(TargetClient)sessionFactory.getCurrentSession().createQuery(hql).setParameter("id", new Integer(targetClientId)).list().get(0);
		sessionFactory.getCurrentSession().delete(targetClient);
	}
	
	@Transactional
	public UserData addTargetClient(){
		String username="user";
		Integer clientId=1;		
		User user = (User) sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("username",username)).list().get(0);
		
			Client client = (Client) sessionFactory.getCurrentSession().load(Client.class, clientId);
				
		
		HashSet targetClientActionPlanSet = new HashSet();
		ActionPlan actionPlan1=new ActionPlan("Get in touch with CMIC CEO");
		ActionPlan actionPlan2=new ActionPlan("Get in touch with CMIC COO");
		targetClientActionPlanSet.add(actionPlan1);
		targetClientActionPlanSet.add(actionPlan2);	
		
		HashSet targetClientHelperSet = new HashSet();
		Helper helper1=new Helper("Target Client Helper 1");
		Helper helper2=new Helper("Target Client Helper 2");
		targetClientHelperSet.add(helper1);
		targetClientHelperSet.add(helper2);
		
		TargetClient tcli=new TargetClient(new BigDecimal("100"), new BigDecimal("200"), client, user);
		tcli.setActionPlans(targetClientActionPlanSet);
		tcli.setHelpers(targetClientHelperSet);
		Integer tcliId=(Integer)sessionFactory.getCurrentSession().save(tcli);		
		return new UserData(tcli, null);
	}
	
	
	@Transactional
	public UserData addTargetClient2(){
		String username="admin";
		User user = (User) sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("username",username)).list().get(0);
		
		String clientName="wipfli";
		Client client = (Client) sessionFactory.getCurrentSession()
				.createCriteria(Client.class)
				.list().get(0);
		
		HashSet targetClientActionPlanSet = new HashSet();
		ActionPlan actionPlan1=new ActionPlan("Get in touch with CMIC CEO");
		ActionPlan actionPlan2=new ActionPlan("Get in touch with CMIC COO");
		targetClientActionPlanSet.add(actionPlan1);
		targetClientActionPlanSet.add(actionPlan2);	
		
		HashSet targetClientHelperSet = new HashSet();
		Helper helper1=new Helper("Target Client Helper 1");
		Helper helper2=new Helper("Target Client Helper 2");
		targetClientHelperSet.add(helper1);
		targetClientHelperSet.add(helper2);
		
		TargetClient tcli=new TargetClient(new BigDecimal("100"), new BigDecimal("200"), client, user);
		tcli.setActionPlans(targetClientActionPlanSet);
		tcli.setHelpers(targetClientHelperSet);
		Integer tcliId=(Integer)sessionFactory.getCurrentSession().save(tcli);
		
		
		HashSet targetProspectActionPlanSet2 = new HashSet();
		ActionPlan plan1=new ActionPlan("Get in touch with Microsoft CEO");
		ActionPlan plan2=new ActionPlan("Get in touch with Microsoft COO");
		targetProspectActionPlanSet2.add(plan1);
		targetProspectActionPlanSet2.add(plan2);	
		
		HashSet targetProspectHelperSet = new HashSet();
		Helper helper3=new Helper("Target Prospect Helper 1");
		Helper helper4=new Helper("Target Prospect Helper 2");
		targetProspectHelperSet.add(helper3);
		targetProspectHelperSet.add(helper4);
		
		TargetProspect tpros=new TargetProspect(new BigDecimal("100"), new BigDecimal("200"),"Microsoft",user);
		tpros.setActionPlans(targetProspectActionPlanSet2);
		tpros.setHelpers(targetProspectHelperSet);
		Integer tprosId=(Integer)sessionFactory.getCurrentSession().save(tpros);
		
		return new UserData(tcli, tpros);
	}

}
