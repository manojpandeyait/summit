package org.wipfli.summit.dao;


import java.util.HashSet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.wipfli.summit.domain.ActionPlan;
import org.wipfli.summit.domain.Client;
import org.wipfli.summit.domain.Helper;
import org.wipfli.summit.domain.Industry;
import org.wipfli.summit.domain.Region;
import org.wipfli.summit.domain.TargetClient;
import org.wipfli.summit.domain.TargetProspect;
import org.wipfli.summit.domain.User;
import org.wipfli.summit.dto.ActionPlanDTO;
import org.wipfli.summit.dto.HelperDTO;
import org.wipfli.summit.dto.TargetClientDTO;
import org.wipfli.summit.dto.TargetProspectDTO;


public class SalesLeadDAOImpl implements SalesLeadDAO{
	private SessionFactory sessionFactory;
	
	public SalesLeadDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public void deleteCollaborator(int id){
		Session session=sessionFactory.getCurrentSession();
		Helper helper=new Helper();
		helper.setId(id);
		session.delete(helper);
	}
	
	
	@Transactional
	public void deleteActionPlan(int id){
		Session session=sessionFactory.getCurrentSession();
		ActionPlan actionPlan=new ActionPlan();
		actionPlan.setId(id);
		session.delete(actionPlan);
	}
	
	@Transactional
	public void deleteTargetClient(int id){
		Session session=sessionFactory.getCurrentSession();
		Object obj=session.load(TargetClient.class, id);
		session.delete(obj);
	}
	
	@Transactional
	public void deleteTargetProspect(int id){
		Session session=sessionFactory.getCurrentSession();
		Object obj=session.load(TargetProspect.class, id);
		session.delete(obj);
	}
	
	
	
	@Transactional
	public void addActionPlan(ActionPlanDTO actionPlanDTO){
		if(actionPlanDTO.getTargetClientId()!=null){
			TargetClient targetClient=(TargetClient)sessionFactory.getCurrentSession().load(TargetClient.class, actionPlanDTO.getTargetClientId());
			ActionPlan actionPlan=new ActionPlan(actionPlanDTO.getDescription());
			targetClient.getActionPlans().add(actionPlan);
		}
	}
	
	@Transactional
	public void addCollaborator(HelperDTO helperDTO){
		if(helperDTO.getTargetClientId()!=null){
			TargetClient targetClient=(TargetClient)sessionFactory.getCurrentSession().load(TargetClient.class, helperDTO.getTargetClientId());
			Helper helper=new Helper(helperDTO.getName());
			targetClient.getHelpers().add(helper);
		
		}
	}
	
	@Transactional
	public void addTargetClient(TargetClientDTO targetClientDTO){
		User user = (User) sessionFactory.getCurrentSession().load(User.class, targetClientDTO.getUserId());
		Client client = (Client) sessionFactory.getCurrentSession().load(Client.class, targetClientDTO.getClientId());
		HashSet<ActionPlan> targetClientActionPlanSet = new HashSet<ActionPlan>();
		HashSet<Helper> targetClientHelperSet = new HashSet<Helper>();
	     
		for(HelperDTO helperDTO: targetClientDTO.getHelpers()){
			Helper helper=new Helper(helperDTO.getName());
			targetClientHelperSet.add(helper);
		}
		
		for(ActionPlanDTO actionPlanDTO: targetClientDTO.getActionPlans()){
			ActionPlan actionPlan= new ActionPlan(actionPlanDTO.getDescription());
			targetClientActionPlanSet.add(actionPlan);
		}
		
		TargetClient targetClient=new TargetClient(targetClientDTO.getCurrentAnnualRevenue(), targetClientDTO.getPotentialAnnualRevenue(), client, user);
		targetClient.setHelpers(targetClientHelperSet);
		targetClient.setActionPlans(targetClientActionPlanSet);
		sessionFactory.getCurrentSession().save(targetClient);	
	}
	
	@Transactional
	public void addTargetProspect(TargetProspectDTO targetProspectDTO){
		Session session=sessionFactory.getCurrentSession();
		User user = (User) session.load(User.class, targetProspectDTO.getUserId());
		Region region=(Region)session.load(Region.class,targetProspectDTO.getRegionId() );
		Industry industry=(Industry)session.load(Industry.class,targetProspectDTO.getIndustryId() );
		
		HashSet<ActionPlan> targetProspectActionPlanSet = new HashSet<ActionPlan>();
		HashSet<Helper> targetProspectHelperSet = new HashSet<Helper>();
	     		
		for(ActionPlanDTO actionPlanDTO: targetProspectDTO.getActionPlans()){
			ActionPlan actionPlan= new ActionPlan(actionPlanDTO.getDescription());
			targetProspectActionPlanSet.add(actionPlan);
		}
		for(HelperDTO helperDTO: targetProspectDTO.getHelpers()){
			Helper helper=new Helper(helperDTO.getName());
			targetProspectHelperSet.add(helper);
		}
	
		
		TargetProspect targetProspect=new TargetProspect(targetProspectDTO.getCurrentAnnualRevenue(), targetProspectDTO.getPotentialAnnualRevenue(),targetProspectDTO.getName(),user);
		targetProspect.setRegion(region);
		targetProspect.setIndustry(industry);
		targetProspect.setHelpers(targetProspectHelperSet);
		targetProspect.setActionPlans(targetProspectActionPlanSet);
		session.save(targetProspect);
	}
}
