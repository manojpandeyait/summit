package org.wipfli.summit.dao;


import org.wipfli.summit.dto.ActionPlanDTO;
import org.wipfli.summit.dto.HelperDTO;
import org.wipfli.summit.dto.TargetClientDTO;
import org.wipfli.summit.dto.TargetProspectDTO;

public interface SalesLeadDAO {
	public void addCollaborator(HelperDTO helperDTO);
	public void addActionPlan(ActionPlanDTO hactionPlanDTO);
	public void addTargetClient(TargetClientDTO targetClientDTO);
	public void addTargetProspect(TargetProspectDTO targetProspectDTO);
	public void deleteCollaborator(int id);
	public void deleteActionPlan(int id);
	public void deleteTargetClient(int id);
	public void deleteTargetProspect(int id);
}
