package org.wipfli.summit.dto;

public class IndustryDTO {
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIpic() {
		return ipic;
	}
	public void setIpic(String ipic) {
		this.ipic = ipic;
	}
	private String name;
	private String ipic;
}
