package org.wipfli.summit.dto;

public class HelperDTO {
	private Integer targetClientId;
	private Integer targetProspectId;
	private String name;
	
	public HelperDTO(){ 	 
    }
	public HelperDTO(String name){ 	 
		this.name=name;
    }
	public Integer getTargetClientId() {
		return targetClientId;
	}
	public void setTargetClientId(Integer targetClientId) {
		this.targetClientId = targetClientId;
	}
	public Integer getTargetProspectId() {
		return targetProspectId;
	}
	public void setTargetProspectId(Integer targetProspectId) {
		this.targetProspectId = targetProspectId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
