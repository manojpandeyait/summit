package org.wipfli.summit.dto;

import java.math.BigDecimal;
import java.util.ArrayList;

public class TargetClientDTO {
	private Integer id;
	private BigDecimal currentAnnualRevenue;
	private BigDecimal potentialAnnualRevenue;
	private Integer clientId;
	private Integer userId;	
	private ArrayList<HelperDTO> helpers;
	private ArrayList<ActionPlanDTO> actionPlans;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public BigDecimal getCurrentAnnualRevenue() {
		return currentAnnualRevenue;
	}
	public void setCurrentAnnualRevenue(BigDecimal currentAnnualRevenue) {
		this.currentAnnualRevenue = currentAnnualRevenue;
	}
	public BigDecimal getPotentialAnnualRevenue() {
		return potentialAnnualRevenue;
	}
	public void setPotentialAnnualRevenue(BigDecimal potentialAnnualRevenue) {
		this.potentialAnnualRevenue = potentialAnnualRevenue;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public ArrayList<HelperDTO> getHelpers() {
		return helpers;
	}
	public void setHelpers(ArrayList<HelperDTO> helpers) {
		this.helpers = helpers;
	}
	public ArrayList<ActionPlanDTO> getActionPlans() {
		return actionPlans;
	}
	public void setActionPlans(ArrayList<ActionPlanDTO> actionPlans) {
		this.actionPlans = actionPlans;
	}	
	
}
