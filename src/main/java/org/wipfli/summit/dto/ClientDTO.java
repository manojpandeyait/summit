package org.wipfli.summit.dto;

import org.wipfli.summit.domain.Industry;
import org.wipfli.summit.domain.Region;

public class ClientDTO {
	private int id;
	private String name;
	private Region region;
	private Industry industry;
	
	public ClientDTO(){
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Region getRegion() {
		return region;
	}
	public void setRegion(Region region) {
		this.region = region;
	}
	public Industry getIndustry() {
		return industry;
	}
	public void setIndustry(Industry industry) {
		this.industry = industry;
	}
}
