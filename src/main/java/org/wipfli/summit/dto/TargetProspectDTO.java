package org.wipfli.summit.dto;

import java.math.BigDecimal;
import java.util.ArrayList;

public class TargetProspectDTO {
	private Integer id;
	private String name;
	private BigDecimal currentAnnualRevenue;
	private BigDecimal potentialAnnualRevenue;
	private Integer userId;	
	private Integer regionId;
	private Integer industryId;
	private ArrayList<HelperDTO> helpers;	
	private ArrayList<ActionPlanDTO> actionPlans;
	
	public TargetProspectDTO(){
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getCurrentAnnualRevenue() {
		return currentAnnualRevenue;
	}
	public void setCurrentAnnualRevenue(BigDecimal currentAnnualRevenue) {
		this.currentAnnualRevenue = currentAnnualRevenue;
	}
	public BigDecimal getPotentialAnnualRevenue() {
		return potentialAnnualRevenue;
	}
	public void setPotentialAnnualRevenue(BigDecimal potentialAnnualRevenue) {
		this.potentialAnnualRevenue = potentialAnnualRevenue;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getRegionId() {
		return regionId;
	}
	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}
	public Integer getIndustryId() {
		return industryId;
	}
	public void setIndustryId(Integer industryId) {
		this.industryId = industryId;
	}
	public ArrayList<HelperDTO> getHelpers() {
		return helpers;
	}
	public void setHelpers(ArrayList<HelperDTO> helpers) {
		this.helpers = helpers;
	}
	public ArrayList<ActionPlanDTO> getActionPlans() {
		return actionPlans;
	}
	public void setActionPlans(ArrayList<ActionPlanDTO> actionPlans) {
		this.actionPlans = actionPlans;
	}
}
