package org.wipfli.summit.dto;

public class ActionPlanDTO {
	private Integer targetClientId;
	private Integer targetProspectId;
	private String description;
	
	public ActionPlanDTO(){
		
	}
   public ActionPlanDTO(String description){
		this.description=description;
	}
	public Integer getTargetClientId() {
		return targetClientId;
	}
	public void setTargetClientId(Integer targetClientId) {
		this.targetClientId = targetClientId;
	}
	public Integer getTargetProspectId() {
		return targetProspectId;
	}
	public void setTargetProspectId(Integer targetProspectId) {
		this.targetProspectId = targetProspectId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
