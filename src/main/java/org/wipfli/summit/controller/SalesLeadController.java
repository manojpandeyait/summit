package org.wipfli.summit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.wipfli.summit.dao.SalesLeadDAO;
import org.wipfli.summit.dto.ActionPlanDTO;
import org.wipfli.summit.dto.HelperDTO;
import org.wipfli.summit.dto.TargetClientDTO;
import org.wipfli.summit.dto.TargetProspectDTO;

@RestController
public class SalesLeadController {

	@Autowired
	private SalesLeadDAO salesLeadDao;	
	
	 @RequestMapping(value="/collaborator/add",method = RequestMethod.POST)	
		public  @ResponseBody void addCollaborator(@RequestBody HelperDTO helperDTO) {			
		salesLeadDao.addCollaborator(helperDTO);			
		}
	 
	 @RequestMapping(value="/actionplan/add",method = RequestMethod.POST)	
		public  @ResponseBody void addActionPlan(@RequestBody ActionPlanDTO actionPlanDTO) {			
		salesLeadDao.addActionPlan(actionPlanDTO);
		}
	 
	
	 @RequestMapping(value="/collaborator/delete/{id}",method = RequestMethod.PUT)	
		public  @ResponseBody void  deleteCollaborator( @PathVariable("id") int id) {	
		 salesLeadDao.deleteCollaborator(id);
	 }
	 @RequestMapping(value="/actionplan/delete/{id}",method = RequestMethod.PUT)	
		public  @ResponseBody void  deleteActionPlan( @PathVariable("id") int id) {	
		 salesLeadDao.deleteActionPlan(id);
	 }
	 
	 @RequestMapping(value="/targetClient/add",method = RequestMethod.POST)	
		public  @ResponseBody void addTargetClient(@RequestBody TargetClientDTO targetClientDTO) {		 
		 salesLeadDao.addTargetClient(targetClientDTO);
		}
	 
	 @RequestMapping(value="/targetProspect/add",method = RequestMethod.POST)	
		public  @ResponseBody void  getTargetProspect(@RequestBody TargetProspectDTO targetProspectDTO) {	
		 salesLeadDao.addTargetProspect(targetProspectDTO);
	 }
	 
	 
	 @RequestMapping(value="/targetClient/delete/{id}",method = RequestMethod.PUT)	
		public  @ResponseBody void  deleteTargetClient( @PathVariable("id") int id) {	
		 salesLeadDao.deleteTargetClient(id);
	 }
	 
	 @RequestMapping(value="/targetProspect/delete/{id}",method = RequestMethod.PUT)	
		public  @ResponseBody void  deleteTargetProspect( @PathVariable("id") int id) {	
		 salesLeadDao.deleteTargetProspect(id);
	 }
	 	 
}
