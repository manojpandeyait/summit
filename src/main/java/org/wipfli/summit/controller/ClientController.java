package org.wipfli.summit.controller;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.wipfli.summit.dao.UserDAO;
import org.wipfli.summit.domain.Client;
import org.wipfli.summit.domain.CommonInterest;
import org.wipfli.summit.domain.Industry;
import org.wipfli.summit.domain.Region;
import org.wipfli.summit.domain.TargetClient;
import org.wipfli.summit.domain.UserDTO;
import org.wipfli.summit.domain.UserData;
import org.wipfli.summit.dto.ActionPlanDTO;
import org.wipfli.summit.dto.ClientDTO;
import org.wipfli.summit.dto.HelperDTO;
import org.wipfli.summit.dto.TargetClientDTO;
import org.wipfli.summit.dto.TargetProspectDTO;


/**
 * Handles requests for the application home page.
 */
@RestController
public class ClientController {
	
	@Autowired
	private UserDAO userDao;
	
	 @RequestMapping(value="/add",method = RequestMethod.GET)
	public  @ResponseBody UserData addTargetClient() {
		UserData tcli= userDao.addTargetClient();
		return tcli;				
	}
	 
	 @RequestMapping(value="/delete",method = RequestMethod.GET)
	public  void deleteTargetClient() {
		 Integer targetClientId=18;
		 userDao.deleteTargetClient(targetClientId);						
	}
	 
	 
	 @RequestMapping(value="/clients",method = RequestMethod.GET,produces = "application/json")
		public  List<Client> getAvailableClients() {
			return userDao.getAvailableClients();
			
						
		}
	 
	 @RequestMapping(value="/regions",method = RequestMethod.GET,produces = "application/json")
		public  List<Region> getRegions() {
			List<Region> listClient= userDao.getRegions();
			return listClient;				
		}
	 
	 @RequestMapping(value="/getupdated",method = RequestMethod.GET)
		public  List<CommonInterest> getPeopleHavingSamePreference() {
			List<CommonInterest> listClient= userDao.getPeopleHavingSamePreference();
			return listClient;				
		}
		 

	 @RequestMapping(value="/region",method = RequestMethod.GET,produces = "application/json")
		public  UserDTO getRegion() {
			UserDTO userdto=new UserDTO();
			userdto.setId(3); 
			 userdto.setName("ND");
			 userdto.setRpic("ND-RPIC");
			 return userdto;
		}
	 
	 @RequestMapping(value="/addRegion",method = RequestMethod.POST)	
		public  @ResponseBody Region addRegion(@RequestBody UserDTO userdto) {
			Region region=new Region();			
			region.setName(userdto.getName());
			region.setRpic(userdto.getRpic());
			userDao.addRegion(region);
			 return region;
		}
	
	 
	 @RequestMapping(value="/targetClient/get",method = RequestMethod.GET)	
		public  @ResponseBody TargetClientDTO  getTargetClient() {		
		 TargetClientDTO targetClientDTO=new TargetClientDTO();
		 targetClientDTO.setUserId(1);
		 targetClientDTO.setClientId(1);
		 targetClientDTO.setCurrentAnnualRevenue(new BigDecimal(100.00));
		 targetClientDTO.setPotentialAnnualRevenue(new BigDecimal(100.00));		
		 ArrayList<HelperDTO> helpers=new ArrayList<HelperDTO>();
		 helpers.add(new HelperDTO("TargetClient Helper1"));
		 helpers.add(new HelperDTO("TargetClient Helper2"));
		 targetClientDTO.setHelpers(helpers);	
		 
		 ArrayList<ActionPlanDTO> actionPlans=new ArrayList<ActionPlanDTO>();
		 actionPlans.add(new ActionPlanDTO("TargetClient ActionPlan1"));
		 actionPlans.add(new ActionPlanDTO("TargetClient ActionPlan2"));
		 targetClientDTO.setActionPlans(actionPlans);
		 return targetClientDTO;
		}
	 
	 @RequestMapping(value="/targetProspect/get",method = RequestMethod.GET)	
		public  @ResponseBody TargetProspectDTO  getTargetProspect() {		
		 TargetProspectDTO targetProspectDTO=new TargetProspectDTO();
		 targetProspectDTO.setUserId(1);
		 targetProspectDTO.setRegionId(1);
		 targetProspectDTO.setIndustryId(1);
		 targetProspectDTO.setName("Microsoft");
		 targetProspectDTO.setCurrentAnnualRevenue(new BigDecimal(500.00));
		 targetProspectDTO.setPotentialAnnualRevenue(new BigDecimal(500.00));		
		 ArrayList<HelperDTO> helpers=new ArrayList<HelperDTO>();
		 helpers.add(new HelperDTO("TargetProspect Helper1"));
		 helpers.add(new HelperDTO("TargetProspect Helper2"));
		 targetProspectDTO.setHelpers(helpers);	
		 
		 ArrayList<ActionPlanDTO> actionPlans=new ArrayList<ActionPlanDTO>();
		 actionPlans.add(new ActionPlanDTO("TargetProspect ActionPlan1"));
		 actionPlans.add(new ActionPlanDTO("TargetProspect ActionPlan2"));
		 targetProspectDTO.setActionPlans(actionPlans);
		 return targetProspectDTO;
		}
}
