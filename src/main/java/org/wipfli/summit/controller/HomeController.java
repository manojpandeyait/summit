package org.wipfli.summit.controller;
import java.util.List;










import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.wipfli.summit.dao.UserDAO;
import org.wipfli.summit.domain.User;

/**
 * Handles requests for the application home page.
 */
@RestController
public class HomeController {
	
	@Autowired
	private UserDAO userDao;
	
	 @RequestMapping(value="/view",method = RequestMethod.GET, produces = "application/json")
	public List<User> home() {
		List<User> listUsers = userDao.list();		
		return listUsers;
	}
	
}
