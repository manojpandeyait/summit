package org.wipfli.summit.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

public class HibernateAwareObjectMapper extends ObjectMapper{
	   public HibernateAwareObjectMapper() {		   
		   Hibernate4Module hbm = new Hibernate4Module();
	        registerModule(hbm);
	    }
}
